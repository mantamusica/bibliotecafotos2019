<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Fotos]].
 *
 * @see Fotos
 */
class FotosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Fotos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Fotos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
