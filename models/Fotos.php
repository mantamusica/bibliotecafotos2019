<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "fotos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $fechaCreado
 * @property string $fechaActualizado
 *
 * @property Tienen[] $tienens
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }
    public $recogerCategorias;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaCreado', 'fechaActualizado'], 'safe'],
            [['nombre'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['nombre'], 'default', 'value' => NULL]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'IdFotos',
            'nombre' => 'Foto',
            'fechaCreado' => 'Fecha Creado',
            'fechaActualizado' => 'Fecha Actualizado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['fotos' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return FotosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FotosQuery(get_called_class());
    }
    public function getImageUrl($id)
    {
        return Url::to('@web/imgs/' . $id.'.jpg',
            true);
    }
    public function upload()
    {
        if ($this->validate()) {
            $id = Fotos::getLasId()->id +1;
            $this->nombre->saveAs('imgs/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function uploadid($id)
    {
        if ($this->validate()) {
            $this->nombre->saveAs('imgs/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function getLasId(){
        return $id=Fotos::find()
            ->select('id')
            ->orderBy(['id' =>SORT_DESC])
            ->limit(1)
            ->one();
    }
    public function  editModel($model){
        $id = Fotos::getLasId()->id +1;
        $model->nombre = $id.'.jpg';
        return $model;

    }
    public function getBotones($data,$i){
        $valor ='';
        if($data[$i]['num'] > 0){
            $valor = $data[$i]['num'];
        }
        return $valor;
    }


}
