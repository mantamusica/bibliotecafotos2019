<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 *
 * @property Tienen[] $tienens
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'],'string', 'max' => 50],
            [['descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'IdCategoria',
            'nombre' => 'Categoria',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['categorias' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return FotosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FotosQuery(get_called_class());
    }

}
