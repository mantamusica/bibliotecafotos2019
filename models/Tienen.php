<?php

namespace app\models;

use Yii;
use app\models\Categorias;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property int $fotos
 * @property int $categorias
 *
 * @property Categorias $categorias0
 * @property Fotos $fotos0
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fotos', 'categorias'], 'integer'],
            [['fotos', 'categorias'], 'unique', 'message' => 'Esta foto ya tiene esta categoría asignada.'],
            [['categorias'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['categorias' => 'id']],
            [['fotos'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['fotos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fotos' => 'Fotos',
            'categorias' => 'Categorias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias0()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'categorias']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotos0()
    {
        return $this->hasOne(Fotos::className(), ['id' => 'fotos']);
    }

    /**
     * {@inheritdoc}
     * @return FotosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FotosQuery(get_called_class());
    }



    public function getCategorias($id)
    {
        $categorias = Self::find()
            ->where(['fotos' => $id])
            ->joinWith('categorias0')
            ->asArray()
            ->all();

        foreach ($categorias as $data){
            $id = $data['categorias0']['nombre'];
            $lista[]=$id;
        }
        return $lista;
    }

}
