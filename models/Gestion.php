<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gestion".
 *
 * @property int $id
 * @property string $nombre
 */
class Gestion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gestion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * {@inheritdoc}
     * @return GestionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GestionQuery(get_called_class());
    }
}
