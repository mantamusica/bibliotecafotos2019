<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Gestion]].
 *
 * @see Gestion
 */
class GestionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Gestion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Gestion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
