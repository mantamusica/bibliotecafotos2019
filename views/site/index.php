<?php
/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 02/04/2019
 * Time: 17:11
 */

/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\helpers\Html;

?>
<div class="site-index">
    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{pager}\n{items}\n{pager}",

            'itemView' => '_thumbnails',
        ]);

        ?>
    </div>
    <div class="row">
        <div class="jumbotron dark">
            <div class="row jumbotron1">
                <h1 class="font-weight-bold">Todas tus fotos</h1>
                <p>Tenemos un total de <?= $totalFotos ?></p>
            </div>
            <div class="row pull-left">
                <?= Html::a('Ver Todas', ['show'], ['class' => 'btn btn-primary btn-sm']) ?>
            </div>

        </div>
    </div>
</div>