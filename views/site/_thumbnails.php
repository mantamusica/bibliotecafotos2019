<?php
/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 02/04/2019
 * Time: 17:11
 */
use yii\helpers\Html;
use app\models\Tienen;
use app\models\Fotos;
/* @var $this yii\web\View */
$url = Fotos::getImageUrl($model->id);
$categorias = Tienen::getCategorias($model->id);
?>
    <div class="col-lg-4">
        <div class="thumbnail">
            <?= Html::a(
            Html::img($url, ['width'=>'300px',['class' => 'img-responsive img-thumbnail']]),
                ['fotos/show', 'id' => $model->id], ['class' => 'profile-link'])
            ; ?>
            <div class="caption">
                <h3>Titulo :<?= $model->nombre; ?></h3>
                <p>Fecha Creación:<?= $model->fechaCreado; ?></p>
                <p>Fecha Actualizado:<?= $model->fechaActualizado; ?></p>
                <h6>
                <?php
                    foreach ($categorias as $categoria){
                        echo $categoria . ', ';
                    }
                ?>
                </h6>
            </div>
        </div>
    </div>