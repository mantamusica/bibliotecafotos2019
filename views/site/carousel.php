<?php
/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 02/04/2019
 * Time: 17:11
 */

/* @var $this yii\web\View */

use yii\bootstrap\Carousel;
use yii\helpers\Html;
use kv4nt\owlcarousel\OwlCarouselWidget;
$this->title = 'Carousel de Fotos';
?>
<div class="site-index">
        <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php
        OwlCarouselWidget::begin([
            'container' => 'div',
            'containerOptions' => [
                'id' => 'container-id',
                'class' => 'container-class'
            ],
            'pluginOptions'    => [
                'autoplay'          => true,
                'autoplayTimeout'   => 3000,
                'items'             => 1,
                'loop'              => true,
                'itemsDesktop'      => [1199, 3],
                'itemsDesktopSmall' => [979, 3]
            ]
        ]);
        ?>

        <div class="item-class"><?= Html::img("@web/imgs/6.jpg")?></div>
        <div class="item-class"><?= Html::img("@web/imgs/14.jpg")?></div>
        <div class="item-class"><?= Html::img("@web/imgs/16.jpg")?></div>
        <div class="item-class"><?= Html::img("@web/imgs/25.jpg")?></div>
        <div class="item-class"><?= Html::img("@web/imgs/32.jpg")?></div>
        <div class="item-class"><?= Html::img("@web/imgs/40.jpg")?></div>


        <?php OwlCarouselWidget::end(); ?>
</div>
</div>