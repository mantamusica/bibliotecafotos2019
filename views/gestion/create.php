<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gestion */

$this->title = 'Nueva Tabla';
$this->params['breadcrumbs'][] = ['label' => 'Gestion Tablas', 'url' => ['crud']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gestion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
