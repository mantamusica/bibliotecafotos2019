<?php
/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 04/04/2019
 * Time: 19:11
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'Gestión de Fotos';
?>
<div class="admin-index">

    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php
        //$form = ActiveForm::begin(); //Default Active Form begin
        $form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'form-vertical',
            ],
        ]);
        echo $form->field($model, 'nombre')->dropDownList(
            $data,
            ['prompt'=>'Escoge ...']
        );
        echo Html::submitButton('Adelante', ['class'=> 'btn btn-success']);
        ActiveForm::end();
        ?>
    </div>
</div>