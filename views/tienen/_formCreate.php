<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categorias;
use app\models\Fotos;
use yii\helpers\ArrayHelper;

$fotos=Fotos::find()->all();
$listFotos=ArrayHelper::map($fotos,'nombre','nombre');
$categorias=Categorias::find()->all();
$listCategorias=ArrayHelper::map($categorias,'nombre','nombre');

/* @var $this yii\web\View */
/* @var $model app\models\Tienen */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="tienen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fotos')->dropDownList(
        $listFotos,
        ['prompt'=>'Selecciona una ...']
    );
    ?>

    <?= $form->field($model, 'categorias')->dropDownList(
        $listCategorias,
        ['prompt'=>'Selecciona una ...']
    );
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
