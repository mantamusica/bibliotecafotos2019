<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tienen */

$this->title = 'Agregar Categorías a las Fotos';
$this->params['breadcrumbs'][] = ['label' => 'Categorías de Fotos', 'url' => ['crud']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formCreate', [
        'model' => $model,
    ]) ?>

</div>
