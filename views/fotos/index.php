<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Fotos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotos-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera Página',
            'lastPageLabel'  => 'Última Página'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'fechaCreado',
            'fechaActualizado',
            [
                'header' => 'Imagen',
                'format'=>'html',
                'value' => function ($model) {
                    $url = $model->getImageUrl($model->id);
                    return Html::img($url, ['width'=>'120','class'=>'img-rounded img-responsive']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Acción',
                'template' => '{link}',
                'buttons' => [
                    'link' => function ($url,$model) {
                        $url = 'show?id=' . $model->id;
                        return Html::a('Ver Detalle', ['show', 'id' => $model->id], ['class' => 'btn btn-danger']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
