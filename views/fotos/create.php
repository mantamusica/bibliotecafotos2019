<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */

$this->title = 'Agregar Nueva Foto';
$this->params['breadcrumbs'][] = ['label' => 'Fotos', 'url' => ['crud']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formCreate', [
        'model' => $model
    ]) ?>

</div>
