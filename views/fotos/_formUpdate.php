<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="fotos-form">

    <div class="row col-lg-6">

        <?php

        Pjax::begin([
        // Pjax options
        ]);

         $form = ActiveForm::begin([
                 'options' => [
                         'enctype' => 'multipart/form-data',
                         'validateOnSubmit' => true,
                         'data' => ['pjax' => true],
                     ]]) ?>

        <?= $form->field($categoria, 'id')->widget(Select2::classname(), [
            'data' => $listaCategorias,
            'options' => ['placeholder' => 'Selecciona una categoría ...', 'multiple' => true, 'maintainOrder' => true],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ])->label('Listado de Categorías');;

        ?>

        <?= $form->field($model, 'fechaCreado')->textInput(['disabled' => true]) ?>

        <?= $form->field($model, 'fechaActualizado')->textInput(['disabled' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>


    </div>
    <div class="row col-lg-6">

            <div class="col-md-12">
                <?= $form->field($model, 'nombre')->fileInput(['class' => 'btn btn-default btn-file']) ?>
                <div class="thumbnail">
                    <?php $url = $model->getImageUrl($model->id);?>
                    <?= Html::img($url, ['width'=>'360px','class' =>'img-responsive img-rounded']); ?>
                </div>
            </div>
            <br>
        <?php

        ActiveForm::end();
        Pjax::end();
        ?>
    </div>

</div>
