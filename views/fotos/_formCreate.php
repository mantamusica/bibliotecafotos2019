<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Categorias;
use kartik\select2\Select2;

$categorias=Categorias::find()->select('id, nombre')->all();
$listaCategorias=ArrayHelper::map($categorias,'id','nombre');

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */
/* @var $form yii\widgets\ActiveForm */
$categoria = new Categorias();
?>

<div class="fotos-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','validateOnSubmit' => true,]]) ?>

    <?= $form->field($model, 'nombre')->fileInput(['class' => 'btn btn-default btn-file','label'=>'Categoria']) ?>

    <?= $form->field($categoria, 'id')->widget(Select2::classname(), [
        'data' => $listaCategorias,
        'language' => 'es',
        'options' => [
            'placeholder' => 'Añadir Categorias',
            'id' => 'nombre',
            'prompt' => 'Selecciona Categoría...',
            'selected' => 'selected'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
            'maximumInputLength' => 10,
            'multiple' => true
        ],
    ]);

    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
