<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gestión de Fotos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Agregar Nueva Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera Página',
            'lastPageLabel'  => 'Última Página'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'fechaCreado',
            'fechaActualizado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
        <?= Html::a( 'Atrás', Yii::$app->request->referrer,['class'=>'btn btn-primary']);?>

    </p>
</div>
