<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Fotos;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$url = \app\models\Fotos::getImageUrl($id);
$this->title = 'Detalle completo de Foto.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotos-index">

    <div class="especial">
        <div class="row col-lg-12 superior">
            <h2>Título: <?= $nombre ?></h2>
            <p>Fecha Creado : <?= $creado ?></p>
            <p>Fecha Actualizado : <?= $actualizado ?></p>
        </div>
        <div class="row col-lg-6">
            <?= Html::a('<h6 class="pull-left">Categorias</h6><h6 class="pull-right">'.$categoriasTotal.'</h6>', ['site/index'], ['class' => 'btn btn-danger botones1']);?>
            <?php
            for ($i = 1; $i <= count($data); $i++) { ?>
                <?= Html::a('<h6 class="pull-left">'.$data[$i]['nombre'].'</h6><h6 class="pull-right">'.Fotos::getBotones($data,$i).'</h6>', ['site/category', 'id' =>$i], ['class' => 'btn btn-primary botones2']);
            }
            ?>
        </div>
        <div class="row col-lg-6 " style="padding-left: 100px;">
            <div class="thumbnail">
                <?= Html::img($url, ['width'=>'100%',['class' => 'img-responsive img-thumbnail']
                ]); ?>
            </div>
        </div>
        <div class="row col-lg-12 superior">
            <p><?= Html::a( 'Atrás', Yii::$app->request->referrer,['class'=>'btn btn-success']);?></p>
        </div>
    </div>
</div>