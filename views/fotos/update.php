<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */

$this->title = 'Actualizar Foto: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gestión de Fotos', 'url' => ['crud']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';

?>
<div class="fotos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formUpdate', [
        'model' => $model,
        'categoria' => $categoria,
        'listaCategorias' => $listaCategorias

    ]) ?>

</div>
