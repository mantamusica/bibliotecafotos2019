<?php

use yii\db\Migration;
use DateTime;

/**
 * Class m190404_112115_fotos
 */
class m190404_112115_fotos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fotos', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'updated_at' => $this->timestamp()->defaultValue(['expression'=>'CURRENT_TIMESTAMP']),
            'created_at' => $this->timestamp()->null()
        ]);
//
//        $this->insert("fotos", [
//            'nombre'=>'1.jpg',
//            'fechaSubida'=>null,
//            'fechaActualizacion'=>null,
//        ]);
//
//        $this->insert("fotos", [
//            'nombre'=>'2.jpg',
//            'fechaSubida'=>null,
//            'fechaActualizacion'=>null,
//        ]);
//        $this->insert("fotos", [
//            'nombre'=>'3.jpg',
//            'fechaSubida'=>null,
//            'fechaActualizacion'=>null,
//        ]);
//        $this->insert("fotos", [
//            'nombre'=>'4.jpg',
//            'fechaSubida'=>null,
//            'fechaActualizacion'=>null,
//        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('fotos');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_112115_fotos cannot be reverted.\n";

        return false;
    }
    */
}
