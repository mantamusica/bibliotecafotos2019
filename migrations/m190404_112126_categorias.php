<?php

use yii\db\Migration;

/**
 * Class m190404_112126_categorias
 */
class m190404_112126_categorias extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categorias', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'descripcion' => $this->string(200)
        ]);

        $this->insert("categorias", [
            'nombre'=>'deportes',
            'descripcion'=>'El deporte es una actividad reglamentada, normalmente de carácter competitivo, que puede mejorar la condición física​ de quien lo practica, y además tiene propiedades que lo diferencian del juego.',
        ]);

        $this->insert("categorias", [
            'nombre'=>'fauna',
            'descripcion'=>'La fauna es el conjunto de especies animales que habitan en una región geográfica, que son propias de un período geológico. Esta depende tanto de factores abióticos como de factores bióticos. Entre éstos sobresalen las relaciones posibles de competencia o de depredación entre las especies.',
        ]);
        $this->insert("categorias", [
            'nombre'=>'guerra',
            'descripcion'=>'La guerra, en su sentido estrictamente técnico, es aquel conflicto social en el que dos o más grupos humanos relativamente masivos —principalmente tribus, sociedades o naciones— se enfrentan de manera violenta, preferiblemente, mediante el uso de armas de toda índole, a menudo con resultado de muerte —individual o colectiva— y daños materiales de una entidad considerable.',
        ]);
        $this->insert("categorias", [
            'nombre'=>'ciudad',
            'descripcion'=>'Una ciudad es un asentamiento de población con atribuciones y funciones políticas, administrativas, económicas y religiosas, a diferencia de los núcleos rurales que carecen de ellas, total o parcialmente. Esto tiene su reflejo material en la presencia de edificios específicos y en su configuración urbanística.',
        ]);
        $this->insert("categorias", [
            'nombre'=>'vegetacion',
            'descripcion'=>'La vegetación es la cobertura de plantas (flora) salvajes o cultivadas que crecen espontáneamente sobre una superficie de suelo o en un medio acuático. Hablamos también de una cubierta vegetal. Su distribución en la Tierra depende de los factores climáticos y de los suelos.',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('categorias');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_112126_categorias cannot be reverted.\n";

        return false;
    }
    */
}
