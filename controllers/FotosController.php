<?php

namespace app\controllers;

use app\models\Categorias;
use app\models\Tienen;
use Yii;
use app\models\Fotos;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FotosController implements the CRUD actions for Fotos model.
 */
class FotosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fotos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotos::find(),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCategory($id)
    {
         return $this->redirect(['site/category', 'id' => $id]);
    }

    public function actionCrud()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotos::find(),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        return $this->render('crud', [
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Displays a single Fotos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fotos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fotos();

        if ($model->load(Yii::$app->request->post())) {
            $model->nombre = UploadedFile::getInstance($model, 'nombre');
            if ($model->nombre == NULL){
                return $this->redirect(['create', 'id' => $model->id]);
            }else{
                if ($model->upload()) {
                    Fotos::editModel($model);
                    $model->validate();
                    $valor = Fotos::getLasId()->id +1;
                    $model->save();
                    if(Yii::$app->request->post()['Categorias'] == '') {
                        $tienen = new Tienen();
                        $tienen->fotos = $valor;
                        $tienen->categorias = 1;
                        $tienen->save();
                    }else{
                        $tienen = new Tienen();
                        $tienen->fotos = $valor;
                        $tienen->categorias = Yii::$app->request->post()['Categorias']['id'];
                        $tienen->save();
                    }
                    return $this->redirect(['show', 'id' => $valor]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Fotos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $categoria = new Categorias();
        $categorias = Categorias::find()->select('categorias.id')->where(['fotos' => $id])->joinWith('tienens')->asArray()->all();
        $listaCategorias = ArrayHelper::map(Categorias::find()->all(), 'id','nombre');

        foreach($categorias as $data){
            $catSelecionadas[] = $data['id'];
        }
        $categoria->id = $catSelecionadas;

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categoria' => $categoria,
            'listaCategorias' => $listaCategorias
        ]);
    }

    /**
     * Deletes an existing Fotos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fotos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fotos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fotos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionShow($id)
    {
        $dataArray = Tienen::find()->where(['fotos' => $id])->joinWith('fotos0')->asArray()->all();
        $numeroCategorias = Tienen::find()->count();

        $fotoId = (int)$dataArray[0]['fotos'];
        $nombre = $dataArray[0]['fotos0']['nombre'];
        $creado = $dataArray[0]['fotos0']['fechaCreado'];
        $actualizado = $dataArray[0]['fotos0']['fechaActualizado'];

        $numCategorias = Categorias::find()->count();

        for ($i = 1; $i <= $numCategorias; $i++) {
            $data[$i]= Categorias::find()->select('nombre')->where(['id' => $i])->asArray()->one();
            $data[$i]['num']= Tienen::find()->where(['categorias' => $i])->count();
        }

        return $this->render("detail",[
            "categoriasTotal"=>$numeroCategorias,
            "id"=>$fotoId,
            "nombre"=>$nombre,
            "creado"=>$creado,
            "actualizado"=>$actualizado,
            "data" => $data
        ]);
    }




}
