<?php

namespace app\controllers;

use app\models\Fotos;
use app\models\Tienen;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $total = Fotos::find()->count();

        $dataProvider = new ActiveDataProvider([
            'query' => Fotos::find(),
            'pagination' => [
                'pageSize' => 3
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'totalFotos' => (int)$total,

        ]);
    }
    public function actionCategory($id)
    {
        $total = Fotos::find()->where(['categorias'=>$id])->joinWith('tienens')->count();

        $dataProvider = new ActiveDataProvider([
            'query' => Fotos::find()->where(['categorias' => $id])->joinWith('tienens'),
            'pagination' => [
                'pageSize' => 3
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'totalFotos' => (int)$total,

        ]);
    }

    public function actionShow()
    {
        return $this->redirect(['fotos/index']);
    }
    public function actionSubir()
    {
        return $this->redirect(['fotos/create']);
    }

    public function actionCarrousel()
    {
        return $this->render('carousel');
    }




}
